---
title: Password Capture
metaTitle: Password Capture | Psono Documentation
meta:
  - name: description
    content: How to setup and use a recovery code
---

# Password Capture

## Preamble

For convenience Psono offers the possibility to capture passwords on the fly and store them in your datastore.

## How does password capture work?

Make sure that you installed the browser extension and that you are logged in. Afterwards the process looks like this:

1)  Login on a website

2)  Accept new entry:

There should be a small browser notification, informing you about the new entry

![Password capture notification](/images/user/password_capture/password_capture_notification.jpg)

3)  Click on "Yes"

The entry is now stored in your datastore


## Potential issues

There are a couple of things that can go wrong, that might lead to the impression that the password capture is not working.

1)  Enable browser notifications

Make sure that you do not block all browser notifications.

2)  Not all websites are supported

Psono supports all websites that use a regular form submit or where Psono can capture the Submit event.
Due to the various techniques and events that exists to submit forms Psono cannot support all websites.

3)  Multi monitor setups

The browser notification is only popping up on one monitor, so if you have multiple monitors attached to it or even a TV,
you might miss the notifications as it is popping up on one of these other monitors too.

4)  No support in Firefox

Psono is using browser notifications to inform users about the potential new entry. Firefox currently does not support
buttons for these notifications, and as such this feature is not working on Firefox.
The official issue can be found here [https://bugzilla.mozilla.org/show_bug.cgi?id=1190681](https://bugzilla.mozilla.org/show_bug.cgi?id=1190681)