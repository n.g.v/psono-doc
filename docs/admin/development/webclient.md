---
title: Webclient
metaTitle: Development Webclient | Psono Documentation
meta:
  - name: description
    content: Getting started guide for the webclient development
---

# Webclient Development

If you want to start to develop own features for the clients, then follow the following steps to setup your own development environment.

## Preamble

This whole guide is based on Ubuntu 18.04 LTS. Ubuntu 12.04+ LTS and Debian based systems should be similar if not even
identical.

We assume that you already have somewhere a Psono server running. If not follow the [guide to setup a Psono Server](/admin/development/datastore-structure.html).

## Installation

1.  Install some generic stuff

    ```bash
    sudo apt-get update
    sudo apt-get install -y git
    ```

2.  Clone git repository

    ```bash
    git clone https://gitlab.com/psono/psono-client.git ~/psono-client
    ```

3.  Checkout new branch

    ```bash
    cd ~/psono-client
    git fetch
    git checkout develop
    git checkout -b [name_of_your_new_branch]
    ```

4.  Install requirements

    ```bash
    sudo var/build-ubuntu.sh
    sudo npm install http-server -g
    ```

## Run the dev server

From this point on forward, you can develop it like any web application.

To start npm's http-server with our web client run the following command:

```bash
http-server ~/psono-client/src/common/data/
```

## Build everything

To build the webclient and browser extensions, use the following command:

```bash
gulp
```

Other useful commands are:

```bash
gulp watch # Will watch for changes and rebuild
gulp sass # Changes in the .sass files will be transpiled to css
gulp template # Changes in src/common/data/view will be compiled to a new template.js
```

## Run Unit Tests (with coverage)

Issue the following command:

```bash
karma start ./unittests/karma-chrome.conf.js
```

if you want to use another browser like firefox you can also use `./unittests/karma-firefox.conf.js` instead or for
something more generic `./unittests/karma-generic.conf.js`. If you use "generic" point the browser of your choice
to the shown url.

or more sexy with gulp:

```bash
gulp unittest
```

or if you want to watch for changes and run it automatically:

```bash
gulp unittestwatch
```

The `./unittests/karma-generic.conf.js` config automatically generates an karma coverage report in html format
in `unittests/coverage`

The output of this command can be shown on http://your-ip/htmlcov/

# Generate javascript docs

To generate the javascript docs run the following command

```bash
gulp docs
```


